using System.ComponentModel;

namespace Wappa.CaptureRideService.Entities
{
    public enum ReasonType
    {
        [Description("INVALID AMOUNT")]
        InvalidAmount,

        [Description("WITHDRAWAL COUNT EXCEEDED")]
        WithdrawalCountExceeded,

        [Description("NOT ENOUGH BALANCE")]
        NotEnoughBalance,

        [Description("FRAUD")]
        Fraud,

        [Description("REFUSED")]
        Refused,

        [Description("ACQUIRER ERROR")]
        AcquirerError,

        [Description("ISSUER UNAVAILABLE")]
        IssuerUnavailable,

        [Description("NOT SUPPORTED")]
        Notsupported,

        [Description("CVC DECLINED")]
        CvcDeclined,

        [Description("REFERRAL")]
        Referral,

        [Description("SERVICEEXCEPTION")]
        ServiceException,

        [Description("TRANSACTION NOT PERMITTED")]
        TransactionNotPermitted,

        [Description("BLOCKED CARD")]
        BlockedCard,

        [Description("DECLINED NON GENERIC")]
        DeclinedNonGeneric,

        [Description("INVALID CARD NUMBER")]
        InvalidCardNumber,

        [Description("EXPIRED CARD")]
        ExpiredCard,

        [Description("RESTRICTED CARD")]
        RestrictedCard,
    }
}