using System.Threading.Tasks;

namespace Wappa.CaptureRideService.SQS
{
    public interface IEventPublisher
    {
        Task PublishEvent<T>(T message);
    }
}